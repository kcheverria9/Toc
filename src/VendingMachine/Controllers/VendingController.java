package VendingMachine.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

import java.util.*;

public class VendingController {
    private Integer currentState = 0;
    private HashMap<Integer,ArrayList<Integer>> automaton;
    @FXML
    private JFXTextArea Cost;
    @FXML
    private JFXTextArea transition;

    @FXML
    private JFXButton FiveCoin;
    @FXML
    private JFXButton TenCoin;
    @FXML
    private JFXButton TwentyCoin;


    private void activateCoins(){
        FiveCoin.setDisable(false);
        TenCoin.setDisable(false);
        TwentyCoin.setDisable(false);
    }
    private void disableCoins(){
            FiveCoin.setDisable(true);
            TenCoin.setDisable(true);
            TwentyCoin.setDisable(true);
        }

    @FXML
    private void onItemAdded(MouseEvent event) {
        Button source = (Button) event.getSource();
        switch (source.getId()) {
            case "BagJuice":
                Cost.clear();
                Cost.setText("Bag Juice - $15");
                currentState = 0;
            transition.clear();
                activateCoins();
                automaton = createBagJuiceAutomaton();
                break;
            case "CranWata":
                Cost.clear();
                Cost.setText("Cranberry Water - $40");
                currentState = 0;
            transition.clear();
                activateCoins();
                automaton = createCranWataAutomaton();
                break;
            case "BigFoot":
                Cost.clear();
                Cost.setText("Big foot - $30");
                currentState = 0;
            transition.clear();
                activateCoins();
                automaton = createBigFootAutomaton();
                break;
            case "Pepsi":
                Cost.clear();
                Cost.setText("Pepsi - $35");
                currentState = 0;
            transition.clear();
                activateCoins();
                automaton = createPepsiAutomaton();
                break;
            case "Doritos":
                Cost.clear();
                Cost.setText("Doritos - $50");
                currentState = 0;
            transition.clear();
                activateCoins();
                automaton = createDoritosAutomaton();
                break;
            default:
                System.out.println("Please press a button");
        }
    }

    public void onCoinClicked(MouseEvent event){
        Button source = (Button) event.getSource();
        System.out.println(source.getText().replace("$",""));
        int coin = Integer.parseInt(source.getText().replace("$",""));
        transition(coin);
        if(automaton.get(currentState).isEmpty()){
            Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
            confirmation.setTitle("You have successfully bought an item");
            if (currentState<0){
                confirmation.setContentText(String.format("Your change is $%s",Integer.toString(currentState).replace("-","")));
            }else {
                confirmation.setContentText("You get back no change");
            }
            disableCoins();
            confirmation.showAndWait();
            currentState = 0;
            transition.clear();
            Cost.clear();
        }
    }

    private HashMap<Integer,ArrayList<Integer>> createBagJuiceAutomaton(){
        HashMap<Integer,ArrayList<Integer>> bagJuice = new HashMap<>();
        bagJuice.put(0, new ArrayList<>(Arrays.asList(5,10,-5)));
        bagJuice.put(5,new ArrayList<>(Arrays.asList(10,15,-1)));
        bagJuice.put(10,new ArrayList<>(Arrays.asList(15,-5,-1)));
        bagJuice.put(15,new ArrayList<>());
        bagJuice.put(-5,new ArrayList<>());

        return bagJuice;
    }
private HashMap<Integer,ArrayList<Integer>> createCranWataAutomaton(){
        HashMap<Integer,ArrayList<Integer>> cranWata = new HashMap<>();
        cranWata.put(0, new ArrayList<>(Arrays.asList(5,10,20)));
        cranWata.put(5,new ArrayList<>(Arrays.asList(10,15,25)));
        cranWata.put(10,new ArrayList<>(Arrays.asList(15,-1,-1)));
        cranWata.put(15,new ArrayList<>(Arrays.asList(20,25,35)));
        cranWata.put(20,new ArrayList<>(Arrays.asList(25,-1,40)));
        cranWata.put(25,new ArrayList<>(Arrays.asList(30,35,-1)));
        cranWata.put(30,new ArrayList<>(Arrays.asList(35,40,-1)));
        cranWata.put(35,new ArrayList<>(Arrays.asList(40,-1,-1)));
        cranWata.put(40,new ArrayList<>());

        return cranWata;
    }
private HashMap<Integer,ArrayList<Integer>> createBigFootAutomaton(){
    HashMap<Integer,ArrayList<Integer>> bigFoot = new HashMap<>();
    bigFoot.put(0, new ArrayList<>(Arrays.asList(5,10,20)));
    bigFoot.put(5,new ArrayList<>(Arrays.asList(10,15,25)));
    bigFoot.put(10,new ArrayList<>(Arrays.asList(5,-1,-1)));
    bigFoot.put(15,new ArrayList<>(Arrays.asList(20,25,-1)));
    bigFoot.put(20,new ArrayList<>(Arrays.asList(25,30,-10)));
    bigFoot.put(25,new ArrayList<>(Arrays.asList(30,-1,-1)));
    bigFoot.put(30,new ArrayList<>());
    bigFoot.put(-10,new ArrayList<>());
        return bigFoot;
    }
private HashMap<Integer,ArrayList<Integer>> createPepsiAutomaton(){
    HashMap<Integer,ArrayList<Integer>> pepsi = new HashMap<>();
    pepsi.put(0, new ArrayList<>(Arrays.asList(5,10,20)));
    pepsi.put(5,new ArrayList<>(Arrays.asList(10,15,25)));
    pepsi.put(10,new ArrayList<>(Arrays.asList(15,20,30)));
    pepsi.put(15,new ArrayList<>(Arrays.asList(20,25,-1)));
    pepsi.put(20,new ArrayList<>(Arrays.asList(25,30,-5)));
    pepsi.put(25,new ArrayList<>(Arrays.asList(30,35,-1)));
    pepsi.put(30,new ArrayList<>(Arrays.asList(35,-5,-1)));
    pepsi.put(35,new ArrayList<>());
    pepsi.put(-5,new ArrayList<>());

        return pepsi;
    }
private HashMap<Integer,ArrayList<Integer>> createDoritosAutomaton(){
        HashMap<Integer,ArrayList<Integer>> doritos = new HashMap<>();
    doritos.put(0, new ArrayList<>(Arrays.asList(5,10,20)));
    doritos.put(5,new ArrayList<>(Arrays.asList(10,15,25)));
    doritos.put(10,new ArrayList<>(Arrays.asList(15,20,-1)));
    doritos.put(15,new ArrayList<>(Arrays.asList(-1,25,35)));
    doritos.put(20,new ArrayList<>(Arrays.asList(25,-1,-1)));
    doritos.put(25,new ArrayList<>(Arrays.asList(30,35,-1)));
    doritos.put(30,new ArrayList<>(Arrays.asList(35,40,50)));
    doritos.put(35,new ArrayList<>(Arrays.asList(40,-1,-1)));
    doritos.put(40,new ArrayList<>(Arrays.asList(45,50,-1)));
    doritos.put(45,new ArrayList<>(Arrays.asList(50,-1,-1)));
    doritos.put(50,new ArrayList<>());
        return doritos;
    }
    private void transition(int coin){
        ArrayList<Integer> transitions = automaton.get(currentState);
        int previousState = currentState;
        String currentTransition = "";
        if (transition.getText().isEmpty()){
            currentTransition = String.format("->%d",previousState);
        }else {
            currentTransition = transition.getText();
        }
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Coin");
        alert.setContentText("Coin has been rejected");
        if (!(transitions.isEmpty())){
            switch (coin){
                case 5 :
                    currentState = transitions.get(0);
                    transition.setText(currentTransition +"->"+ currentState);
                    System.out.println("Current State is: "+currentState);
                    break;
                case 10:
                    if(transitions.get(1)!=-1){
                        currentState = transitions.get(1);
                        transition.setText(currentTransition +"->"+ currentState);
                        System.out.println("Current State is: "+currentState);
                    }else {
                        alert.showAndWait();
                    }
                    break;
                case 20:
                    if (transitions.get(2)!=-1){
                        currentState = transitions.get(2);
                        transition.setText(currentTransition +"->"+ currentState);
                        System.out.println("Current State is: "+currentState);
                    }else {
                        alert.showAndWait();
                    }
                    break;
            }
        }
    }
}
